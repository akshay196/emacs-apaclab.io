HUGO ?= hugo
FILE_NAME ?= $(shell date "+%B-%Y" | tr '[:upper:]' '[:lower:]').md
ANN_DIR = content/announcements
NOTES_DIR = content/notes

build:
	$(HUGO) --gc --minify

announcement:
	@echo "Creating announcement file in '$(ANN_DIR)'."
	mkdir --parents $(ANN_DIR)
	$(HUGO) new $(ANN_DIR)/$(FILE_NAME) --kind "announcement"

notes:
	@echo "Creating notes file in '$(NOTES_DIR)'"
	mkdir --parents $(NOTES_DIR)
	$(HUGO) new $(NOTES_DIR)/$(FILE_NAME) --kind "notes"

server:
	$(HUGO) server --buildDrafts --buildFuture
