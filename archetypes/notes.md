---
title: "Notes from Emacs Asia-Pacific (APAC) virtual meetup, {{ now.Format "January 2006" }}"
linktitle: "Notes from {{ now.Format "January 2006" }} virtual meetup"
date: {{ .Date }}
categories:
- Event
---

These are the notes from [Emacs Asia-Pacific (APAC) meetup happened on
March 27, 2021](https://emacs-apac.gitlab.io/announcements/{{ now.Format "January" | lower }}-{{ now.Format "2006" }}/).

<!-- - Topic of the discussion -->
<!--   - Link1 -->
<!--   - Link2 -->

Credits: [names of the people who participated in the discussion, chat / voice].
